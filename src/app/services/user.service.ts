import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _url: string = "http://localhost:3000/contacts";
  private contactsCount = 1000;

  constructor(private http: HttpClient) {
  }

  /**
   * HTTP, Obserables and RxJS
   * 
   * 1. HTTP Get request from UserService
   * 2. Receive the obserable and cast it into an user array
   * 3. Subscribe to the obserable from userService (in contact component?)
   * 4. Assign the employee array to a local variable
   * 
   * RxJs
   * 
   * 1. Reactive Extensions for JavaScript
   * 2. External library to work with Observables
  */
  
  /**
   * Implement addContacts method using HttpClient for a saving a Contacts details
   * 
   * @param contact 
   * @returns 
  */
  addContact(contact): Observable<any> {
    let payload = {
      "name": contact.name,
      "mobile": contact.mobile,
      "id": this.contactsCount + 1
    };

    let payloadJSON = JSON.stringify(payload);

    if (contact != null) {
      const headers = { 'content-type': 'application/json' };
      // console.log("Payload => Name:" + contact.name + " | Mobile:" + contact.mobile);
      // console.log("this._url: " + this._url);
      // console.log("payloadJSON: " + payloadJSON);

      let result = this.http.post(this._url, payloadJSON, { 'headers': headers });

      return result;
    }

    return null;
  }

  /**
   * Implement getAllContactss method using HttpClient for getting all Contacts details
   * @returns 
   */
  getAllContacts(): Observable<any> {     
    return this.http.get(this._url);
  }
}
