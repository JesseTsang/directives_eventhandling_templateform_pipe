import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  constructor() {}

  ngOnInit() {
  }

}

// export class AppComponent {
//   title = 'mc-angular-spa-development-assignment1';
// }
